﻿' Joshua M. Hughes
' Programming Part 4: [Chapter 3 : Exercise 4] Automobile Cost Calculator
' 7/22/2014
' 
' The application requests input from the user for the dollar amount spent
' per month on Loan Payment, Insurance, Gas, Oil, Tires, and Maintenance. 
' The application will calculate the annual cost for each expense and
' the total annual cost of all automobile expenses. 
'
' Retrieved icon file from: http://www.softicons.com/web-icons/web-orange-buttons-by-axialis-team/car-icon

Public Class AutomobileCosts_Form
    ' Validate entries for each text field on Leave. Entry must be numeric. 
    ' If not numeric, clear the contents of the text field and set focus for re-entry.
    Private Sub LoanPayment_TextBox_Leave(sender As Object, e As EventArgs) Handles LoanPayment_TextBox.Leave
        If LoanPayment_TextBox.Text <> Nothing And IsNumeric(LoanPayment_TextBox.Text) <> True Then
            MsgBox("Enter a valid dollar amount for 'Loan Payment': 0.00.", MsgBoxStyle.Exclamation, AcceptButton)
            LoanPayment_TextBox.Text = Nothing
            LoanPayment_TextBox.Focus()
        End If

    End Sub

    Private Sub Insurance_TextBox_Leave(sender As Object, e As EventArgs) Handles Insurance_TextBox.Leave
        If Insurance_TextBox.Text <> Nothing And IsNumeric(Insurance_TextBox.Text) <> True Then
            MsgBox("Enter a valid dollar amount for 'Insurance': 0.00.", MsgBoxStyle.Exclamation, AcceptButton)
            Insurance_TextBox.Text = Nothing
            Insurance_TextBox.Focus()
        End If

    End Sub

    Private Sub Gas_TextBox_Leave(sender As Object, e As EventArgs) Handles Gas_TextBox.Leave
        If Gas_TextBox.Text <> Nothing And IsNumeric(Gas_TextBox.Text) <> True Then
            MsgBox("Enter a valid dollar amount for 'Gas': 0.00.", MsgBoxStyle.Exclamation, AcceptButton)
            Gas_TextBox.Text = Nothing
            Gas_TextBox.Focus()
        End If

    End Sub

    Private Sub Oil_TextBox_Leave(sender As Object, e As EventArgs) Handles Oil_TextBox.Leave
        If Oil_TextBox.Text <> Nothing And IsNumeric(Oil_TextBox.Text) <> True Then
            MsgBox("Enter a valid dollar amount for 'Oil': 0.00.", MsgBoxStyle.Exclamation, AcceptButton)
            Oil_TextBox.Text = Nothing
            Oil_TextBox.Focus()
        End If

    End Sub

    Private Sub Tires_TextBox_Leave(sender As Object, e As EventArgs) Handles Tires_TextBox.Leave
        If Tires_TextBox.Text <> Nothing And IsNumeric(Tires_TextBox.Text) <> True Then
            MsgBox("Enter a valid dollar amount for 'Tires': 0.00.", MsgBoxStyle.Exclamation, AcceptButton)
            Tires_TextBox.Text = Nothing
            Tires_TextBox.Focus()
        End If

    End Sub

    Private Sub Maintenance_TextBox_Leave(sender As Object, e As EventArgs) Handles Maintenance_TextBox.Leave
        If Maintenance_TextBox.Text <> Nothing And IsNumeric(Maintenance_TextBox.Text) <> True Then
            MsgBox("Enter a valid dollar amount for 'Maintenance': 0.00.", MsgBoxStyle.Exclamation, AcceptButton)
            Maintenance_TextBox.Text = Nothing
            Maintenance_TextBox.Focus()
        End If

    End Sub

    Private Sub CalculateCost_Button_Click(sender As Object, e As EventArgs) Handles CalculateCost_Button.Click
        ' If a field is blank, set to $0.00 to complete calculations
        If LoanPayment_TextBox.Text = Nothing Then
            LoanPayment_TextBox.Text = "0.00"
        End If
        If Insurance_TextBox.Text = Nothing Then
            Insurance_TextBox.Text = "0.00"
        End If
        If Gas_TextBox.Text = Nothing Then
            Gas_TextBox.Text = "0.00"
        End If
        If Oil_TextBox.Text = Nothing Then
            Oil_TextBox.Text = "0.00"
        End If
        If Tires_TextBox.Text = Nothing Then
            Tires_TextBox.Text = "0.00"
        End If
        If Maintenance_TextBox.Text = Nothing Then
            Maintenance_TextBox.Text = "0.00"
        End If

        ' Calculate and display annual costs
        LoanPaymentAnnualDisplay_Label.Text = "$" & CStr(CDec(LoanPayment_TextBox.Text) * 12)
        InsuranceAnnualDisplay_Label.Text = "$" & CStr(CDec(Insurance_TextBox.Text) * 12)
        GasAnnualDisplay_Label.Text = "$" & CStr(CDec(Gas_TextBox.Text) * 12)
        OilAnnualDisplay_Label.Text = "$" & CStr(CDec(Oil_TextBox.Text) * 12)
        TiresAnnualDisplay_Label.Text = "$" & CStr(CDec(Tires_TextBox.Text) * 12)
        MaintenanceAnnualDIsplay_Label.Text = "$" & CStr(CDec(Maintenance_TextBox.Text) * 12)

        ' Calculate and display total annual cost
        TotalAnnualCostDisplay_Label.Text = "$" & CStr(CDec(LoanPaymentAnnualDisplay_Label.Text) _
            + CDec(InsuranceAnnualDisplay_Label.Text) + CDec(GasAnnualDisplay_Label.Text) _
            + CDec(OilAnnualDisplay_Label.Text) + CDec(TiresAnnualDisplay_Label.Text) _
            + CDec(MaintenanceAnnualDIsplay_Label.Text))

    End Sub

    Private Sub Reset_Button_Click(sender As Object, e As EventArgs) Handles Reset_Button.Click
        ' Set values for all text boxes to Nothing. 
        LoanPayment_TextBox.Text = Nothing
        Insurance_TextBox.Text = Nothing
        Gas_TextBox.Text = Nothing
        Oil_TextBox.Text = Nothing
        Tires_TextBox.Text = Nothing
        Maintenance_TextBox.Text = Nothing

        ' Set values for all labels to $0.00
        LoanPaymentAnnualDisplay_Label.Text = "$0.00"
        InsuranceAnnualDisplay_Label.Text = "$0.00"
        GasAnnualDisplay_Label.Text = "$0.00"
        OilAnnualDisplay_Label.Text = "$0.00"
        TiresAnnualDisplay_Label.Text = "$0.00"
        MaintenanceAnnualDIsplay_Label.Text = "$0.00"
        TotalAnnualCostDisplay_Label.Text = "$0.00"

    End Sub

    Private Sub Exit_Button_Click(sender As Object, e As EventArgs) Handles Exit_Button.Click
        ' Close the application
        Me.Close()

    End Sub


End Class
