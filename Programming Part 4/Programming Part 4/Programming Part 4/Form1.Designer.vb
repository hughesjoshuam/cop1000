﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AutomobileCosts_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AutomobileCosts_Form))
        Me.ValueInstructions_Label = New System.Windows.Forms.Label()
        Me.LoanPaymentEnter_Label = New System.Windows.Forms.Label()
        Me.InsuranceEnter_Label = New System.Windows.Forms.Label()
        Me.GasEnter_Label = New System.Windows.Forms.Label()
        Me.OilEnter_Label = New System.Windows.Forms.Label()
        Me.TiresEnter_Label = New System.Windows.Forms.Label()
        Me.MaintenanceEnter_Label = New System.Windows.Forms.Label()
        Me.LoanPayment_TextBox = New System.Windows.Forms.TextBox()
        Me.Insurance_TextBox = New System.Windows.Forms.TextBox()
        Me.Gas_TextBox = New System.Windows.Forms.TextBox()
        Me.Oil_TextBox = New System.Windows.Forms.TextBox()
        Me.Tires_TextBox = New System.Windows.Forms.TextBox()
        Me.Maintenance_TextBox = New System.Windows.Forms.TextBox()
        Me.AnnualDisplay_Label = New System.Windows.Forms.Label()
        Me.LoanPaymentAnnualDisplay_Label = New System.Windows.Forms.Label()
        Me.InsuranceAnnualDisplay_Label = New System.Windows.Forms.Label()
        Me.GasAnnualDisplay_Label = New System.Windows.Forms.Label()
        Me.OilAnnualDisplay_Label = New System.Windows.Forms.Label()
        Me.TiresAnnualDisplay_Label = New System.Windows.Forms.Label()
        Me.MaintenanceAnnualDIsplay_Label = New System.Windows.Forms.Label()
        Me.MonthlyDisplay_Label = New System.Windows.Forms.Label()
        Me.CalculateCost_Button = New System.Windows.Forms.Button()
        Me.Reset_Button = New System.Windows.Forms.Button()
        Me.Exit_Button = New System.Windows.Forms.Button()
        Me.TotalAnnualCost_Label = New System.Windows.Forms.Label()
        Me.TotalAnnualCostDisplay_Label = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ValueInstructions_Label
        '
        Me.ValueInstructions_Label.AutoSize = True
        Me.ValueInstructions_Label.Location = New System.Drawing.Point(12, 9)
        Me.ValueInstructions_Label.Name = "ValueInstructions_Label"
        Me.ValueInstructions_Label.Size = New System.Drawing.Size(228, 13)
        Me.ValueInstructions_Label.TabIndex = 0
        Me.ValueInstructions_Label.Text = "Enter the cost per month for each value below:"
        '
        'LoanPaymentEnter_Label
        '
        Me.LoanPaymentEnter_Label.AutoSize = True
        Me.LoanPaymentEnter_Label.Location = New System.Drawing.Point(17, 52)
        Me.LoanPaymentEnter_Label.Name = "LoanPaymentEnter_Label"
        Me.LoanPaymentEnter_Label.Size = New System.Drawing.Size(75, 13)
        Me.LoanPaymentEnter_Label.TabIndex = 1
        Me.LoanPaymentEnter_Label.Text = "Loan Payment"
        '
        'InsuranceEnter_Label
        '
        Me.InsuranceEnter_Label.AutoSize = True
        Me.InsuranceEnter_Label.Location = New System.Drawing.Point(17, 78)
        Me.InsuranceEnter_Label.Name = "InsuranceEnter_Label"
        Me.InsuranceEnter_Label.Size = New System.Drawing.Size(54, 13)
        Me.InsuranceEnter_Label.TabIndex = 2
        Me.InsuranceEnter_Label.Text = "Insurance"
        '
        'GasEnter_Label
        '
        Me.GasEnter_Label.AutoSize = True
        Me.GasEnter_Label.Location = New System.Drawing.Point(17, 104)
        Me.GasEnter_Label.Name = "GasEnter_Label"
        Me.GasEnter_Label.Size = New System.Drawing.Size(26, 13)
        Me.GasEnter_Label.TabIndex = 3
        Me.GasEnter_Label.Text = "Gas"
        '
        'OilEnter_Label
        '
        Me.OilEnter_Label.AutoSize = True
        Me.OilEnter_Label.Location = New System.Drawing.Point(17, 130)
        Me.OilEnter_Label.Name = "OilEnter_Label"
        Me.OilEnter_Label.Size = New System.Drawing.Size(19, 13)
        Me.OilEnter_Label.TabIndex = 4
        Me.OilEnter_Label.Text = "Oil"
        '
        'TiresEnter_Label
        '
        Me.TiresEnter_Label.AutoSize = True
        Me.TiresEnter_Label.Location = New System.Drawing.Point(17, 156)
        Me.TiresEnter_Label.Name = "TiresEnter_Label"
        Me.TiresEnter_Label.Size = New System.Drawing.Size(30, 13)
        Me.TiresEnter_Label.TabIndex = 5
        Me.TiresEnter_Label.Text = "Tires"
        '
        'MaintenanceEnter_Label
        '
        Me.MaintenanceEnter_Label.AutoSize = True
        Me.MaintenanceEnter_Label.Location = New System.Drawing.Point(17, 182)
        Me.MaintenanceEnter_Label.Name = "MaintenanceEnter_Label"
        Me.MaintenanceEnter_Label.Size = New System.Drawing.Size(69, 13)
        Me.MaintenanceEnter_Label.TabIndex = 6
        Me.MaintenanceEnter_Label.Text = "Maintenance"
        '
        'LoanPayment_TextBox
        '
        Me.LoanPayment_TextBox.Location = New System.Drawing.Point(117, 49)
        Me.LoanPayment_TextBox.Name = "LoanPayment_TextBox"
        Me.LoanPayment_TextBox.Size = New System.Drawing.Size(60, 20)
        Me.LoanPayment_TextBox.TabIndex = 1
        '
        'Insurance_TextBox
        '
        Me.Insurance_TextBox.Location = New System.Drawing.Point(117, 75)
        Me.Insurance_TextBox.Name = "Insurance_TextBox"
        Me.Insurance_TextBox.Size = New System.Drawing.Size(60, 20)
        Me.Insurance_TextBox.TabIndex = 2
        '
        'Gas_TextBox
        '
        Me.Gas_TextBox.Location = New System.Drawing.Point(117, 101)
        Me.Gas_TextBox.Name = "Gas_TextBox"
        Me.Gas_TextBox.Size = New System.Drawing.Size(60, 20)
        Me.Gas_TextBox.TabIndex = 3
        '
        'Oil_TextBox
        '
        Me.Oil_TextBox.Location = New System.Drawing.Point(117, 127)
        Me.Oil_TextBox.Name = "Oil_TextBox"
        Me.Oil_TextBox.Size = New System.Drawing.Size(60, 20)
        Me.Oil_TextBox.TabIndex = 4
        '
        'Tires_TextBox
        '
        Me.Tires_TextBox.Location = New System.Drawing.Point(117, 153)
        Me.Tires_TextBox.Name = "Tires_TextBox"
        Me.Tires_TextBox.Size = New System.Drawing.Size(60, 20)
        Me.Tires_TextBox.TabIndex = 5
        '
        'Maintenance_TextBox
        '
        Me.Maintenance_TextBox.Location = New System.Drawing.Point(117, 179)
        Me.Maintenance_TextBox.Name = "Maintenance_TextBox"
        Me.Maintenance_TextBox.Size = New System.Drawing.Size(60, 20)
        Me.Maintenance_TextBox.TabIndex = 6
        '
        'AnnualDisplay_Label
        '
        Me.AnnualDisplay_Label.AutoSize = True
        Me.AnnualDisplay_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnnualDisplay_Label.Location = New System.Drawing.Point(205, 27)
        Me.AnnualDisplay_Label.Name = "AnnualDisplay_Label"
        Me.AnnualDisplay_Label.Size = New System.Drawing.Size(75, 13)
        Me.AnnualDisplay_Label.TabIndex = 14
        Me.AnnualDisplay_Label.Text = "Annual Cost"
        '
        'LoanPaymentAnnualDisplay_Label
        '
        Me.LoanPaymentAnnualDisplay_Label.AutoSize = True
        Me.LoanPaymentAnnualDisplay_Label.Location = New System.Drawing.Point(223, 52)
        Me.LoanPaymentAnnualDisplay_Label.Name = "LoanPaymentAnnualDisplay_Label"
        Me.LoanPaymentAnnualDisplay_Label.Size = New System.Drawing.Size(34, 13)
        Me.LoanPaymentAnnualDisplay_Label.TabIndex = 16
        Me.LoanPaymentAnnualDisplay_Label.Text = "$0.00"
        '
        'InsuranceAnnualDisplay_Label
        '
        Me.InsuranceAnnualDisplay_Label.AutoSize = True
        Me.InsuranceAnnualDisplay_Label.Location = New System.Drawing.Point(223, 78)
        Me.InsuranceAnnualDisplay_Label.Name = "InsuranceAnnualDisplay_Label"
        Me.InsuranceAnnualDisplay_Label.Size = New System.Drawing.Size(34, 13)
        Me.InsuranceAnnualDisplay_Label.TabIndex = 21
        Me.InsuranceAnnualDisplay_Label.Text = "$0.00"
        '
        'GasAnnualDisplay_Label
        '
        Me.GasAnnualDisplay_Label.AutoSize = True
        Me.GasAnnualDisplay_Label.Location = New System.Drawing.Point(223, 104)
        Me.GasAnnualDisplay_Label.Name = "GasAnnualDisplay_Label"
        Me.GasAnnualDisplay_Label.Size = New System.Drawing.Size(34, 13)
        Me.GasAnnualDisplay_Label.TabIndex = 22
        Me.GasAnnualDisplay_Label.Text = "$0.00"
        '
        'OilAnnualDisplay_Label
        '
        Me.OilAnnualDisplay_Label.AutoSize = True
        Me.OilAnnualDisplay_Label.Location = New System.Drawing.Point(223, 130)
        Me.OilAnnualDisplay_Label.Name = "OilAnnualDisplay_Label"
        Me.OilAnnualDisplay_Label.Size = New System.Drawing.Size(34, 13)
        Me.OilAnnualDisplay_Label.TabIndex = 23
        Me.OilAnnualDisplay_Label.Text = "$0.00"
        '
        'TiresAnnualDisplay_Label
        '
        Me.TiresAnnualDisplay_Label.AutoSize = True
        Me.TiresAnnualDisplay_Label.Location = New System.Drawing.Point(223, 156)
        Me.TiresAnnualDisplay_Label.Name = "TiresAnnualDisplay_Label"
        Me.TiresAnnualDisplay_Label.Size = New System.Drawing.Size(34, 13)
        Me.TiresAnnualDisplay_Label.TabIndex = 24
        Me.TiresAnnualDisplay_Label.Text = "$0.00"
        '
        'MaintenanceAnnualDIsplay_Label
        '
        Me.MaintenanceAnnualDIsplay_Label.AutoSize = True
        Me.MaintenanceAnnualDIsplay_Label.Location = New System.Drawing.Point(223, 182)
        Me.MaintenanceAnnualDIsplay_Label.Name = "MaintenanceAnnualDIsplay_Label"
        Me.MaintenanceAnnualDIsplay_Label.Size = New System.Drawing.Size(34, 13)
        Me.MaintenanceAnnualDIsplay_Label.TabIndex = 25
        Me.MaintenanceAnnualDIsplay_Label.Text = "$0.00"
        '
        'MonthlyDisplay_Label
        '
        Me.MonthlyDisplay_Label.AutoSize = True
        Me.MonthlyDisplay_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MonthlyDisplay_Label.Location = New System.Drawing.Point(109, 27)
        Me.MonthlyDisplay_Label.Name = "MonthlyDisplay_Label"
        Me.MonthlyDisplay_Label.Size = New System.Drawing.Size(80, 13)
        Me.MonthlyDisplay_Label.TabIndex = 26
        Me.MonthlyDisplay_Label.Text = "Monthly Cost"
        '
        'CalculateCost_Button
        '
        Me.CalculateCost_Button.Location = New System.Drawing.Point(39, 238)
        Me.CalculateCost_Button.Name = "CalculateCost_Button"
        Me.CalculateCost_Button.Size = New System.Drawing.Size(75, 23)
        Me.CalculateCost_Button.TabIndex = 7
        Me.CalculateCost_Button.Text = "Calculate Cost"
        Me.CalculateCost_Button.UseVisualStyleBackColor = True
        '
        'Reset_Button
        '
        Me.Reset_Button.Location = New System.Drawing.Point(120, 238)
        Me.Reset_Button.Name = "Reset_Button"
        Me.Reset_Button.Size = New System.Drawing.Size(75, 23)
        Me.Reset_Button.TabIndex = 8
        Me.Reset_Button.Text = "Reset"
        Me.Reset_Button.UseVisualStyleBackColor = True
        '
        'Exit_Button
        '
        Me.Exit_Button.Location = New System.Drawing.Point(201, 238)
        Me.Exit_Button.Name = "Exit_Button"
        Me.Exit_Button.Size = New System.Drawing.Size(75, 23)
        Me.Exit_Button.TabIndex = 9
        Me.Exit_Button.Text = "Exit"
        Me.Exit_Button.UseVisualStyleBackColor = True
        '
        'TotalAnnualCost_Label
        '
        Me.TotalAnnualCost_Label.AutoSize = True
        Me.TotalAnnualCost_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalAnnualCost_Label.Location = New System.Drawing.Point(90, 209)
        Me.TotalAnnualCost_Label.Name = "TotalAnnualCost_Label"
        Me.TotalAnnualCost_Label.Size = New System.Drawing.Size(108, 13)
        Me.TotalAnnualCost_Label.TabIndex = 30
        Me.TotalAnnualCost_Label.Text = "Total Annual Cost"
        '
        'TotalAnnualCostDisplay_Label
        '
        Me.TotalAnnualCostDisplay_Label.AutoSize = True
        Me.TotalAnnualCostDisplay_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TotalAnnualCostDisplay_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalAnnualCostDisplay_Label.Location = New System.Drawing.Point(204, 207)
        Me.TotalAnnualCostDisplay_Label.Name = "TotalAnnualCostDisplay_Label"
        Me.TotalAnnualCostDisplay_Label.Size = New System.Drawing.Size(41, 15)
        Me.TotalAnnualCostDisplay_Label.TabIndex = 31
        Me.TotalAnnualCostDisplay_Label.Text = "$0.00"
        '
        'AutomobileCosts_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 281)
        Me.ControlBox = False
        Me.Controls.Add(Me.TotalAnnualCostDisplay_Label)
        Me.Controls.Add(Me.TotalAnnualCost_Label)
        Me.Controls.Add(Me.Exit_Button)
        Me.Controls.Add(Me.Reset_Button)
        Me.Controls.Add(Me.CalculateCost_Button)
        Me.Controls.Add(Me.MonthlyDisplay_Label)
        Me.Controls.Add(Me.MaintenanceAnnualDIsplay_Label)
        Me.Controls.Add(Me.TiresAnnualDisplay_Label)
        Me.Controls.Add(Me.OilAnnualDisplay_Label)
        Me.Controls.Add(Me.GasAnnualDisplay_Label)
        Me.Controls.Add(Me.InsuranceAnnualDisplay_Label)
        Me.Controls.Add(Me.LoanPaymentAnnualDisplay_Label)
        Me.Controls.Add(Me.AnnualDisplay_Label)
        Me.Controls.Add(Me.Maintenance_TextBox)
        Me.Controls.Add(Me.Tires_TextBox)
        Me.Controls.Add(Me.Oil_TextBox)
        Me.Controls.Add(Me.Gas_TextBox)
        Me.Controls.Add(Me.Insurance_TextBox)
        Me.Controls.Add(Me.LoanPayment_TextBox)
        Me.Controls.Add(Me.MaintenanceEnter_Label)
        Me.Controls.Add(Me.TiresEnter_Label)
        Me.Controls.Add(Me.OilEnter_Label)
        Me.Controls.Add(Me.GasEnter_Label)
        Me.Controls.Add(Me.InsuranceEnter_Label)
        Me.Controls.Add(Me.LoanPaymentEnter_Label)
        Me.Controls.Add(Me.ValueInstructions_Label)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AutomobileCosts_Form"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Automobile Cost Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ValueInstructions_Label As System.Windows.Forms.Label
    Friend WithEvents LoanPaymentEnter_Label As System.Windows.Forms.Label
    Friend WithEvents InsuranceEnter_Label As System.Windows.Forms.Label
    Friend WithEvents GasEnter_Label As System.Windows.Forms.Label
    Friend WithEvents OilEnter_Label As System.Windows.Forms.Label
    Friend WithEvents TiresEnter_Label As System.Windows.Forms.Label
    Friend WithEvents MaintenanceEnter_Label As System.Windows.Forms.Label
    Friend WithEvents LoanPayment_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Insurance_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Gas_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Oil_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Tires_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Maintenance_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents AnnualDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents LoanPaymentAnnualDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents InsuranceAnnualDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents GasAnnualDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents OilAnnualDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents TiresAnnualDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents MaintenanceAnnualDIsplay_Label As System.Windows.Forms.Label
    Friend WithEvents MonthlyDisplay_Label As System.Windows.Forms.Label
    Friend WithEvents CalculateCost_Button As System.Windows.Forms.Button
    Friend WithEvents Reset_Button As System.Windows.Forms.Button
    Friend WithEvents Exit_Button As System.Windows.Forms.Button
    Friend WithEvents TotalAnnualCost_Label As System.Windows.Forms.Label
    Friend WithEvents TotalAnnualCostDisplay_Label As System.Windows.Forms.Label

End Class
