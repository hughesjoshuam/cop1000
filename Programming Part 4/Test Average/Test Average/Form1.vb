﻿' Joshua M. Hughes
' Programming Part 4: The Test Average Program
' 7/21/2014

Public Class Form1

    Private Sub calcButton_Click(sender As Object, e As EventArgs) Handles calcButton.Click
        ' Declare local variables to hold the test scores
        ' and the average.
        Dim test1, test2, test3, average As Double

        ' Get the first test score.
        test1 = CDbl(test1TextBox.Text)

        ' Get the second test score.
        test2 = CDbl(test2TextBox.Text)

        ' Get the third test score.
        test3 = CDbl(test3TextBox.Text)

        ' Calculate the average test score.
        average = (test1 + test2 + test3) / 3

        ' Display the avergae test score in the
        ' averageLabel control
        averageLabel.Text = average.ToString()
    End Sub

    Private Sub exitButton_Click(sender As Object, e As EventArgs) Handles exitButton.Click
        Me.Close()
    End Sub
End Class
