﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.test1Label = New System.Windows.Forms.Label()
        Me.test2Label = New System.Windows.Forms.Label()
        Me.test3Label = New System.Windows.Forms.Label()
        Me.test1TextBox = New System.Windows.Forms.TextBox()
        Me.test2TextBox = New System.Windows.Forms.TextBox()
        Me.test3TextBox = New System.Windows.Forms.TextBox()
        Me.resultLabel = New System.Windows.Forms.Label()
        Me.averageLabel = New System.Windows.Forms.Label()
        Me.calcButton = New System.Windows.Forms.Button()
        Me.exitButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'test1Label
        '
        Me.test1Label.AutoSize = True
        Me.test1Label.Location = New System.Drawing.Point(12, 20)
        Me.test1Label.Name = "test1Label"
        Me.test1Label.Size = New System.Drawing.Size(126, 13)
        Me.test1Label.TabIndex = 0
        Me.test1Label.Text = "Enter the score for test 1:"
        '
        'test2Label
        '
        Me.test2Label.AutoSize = True
        Me.test2Label.Location = New System.Drawing.Point(12, 47)
        Me.test2Label.Name = "test2Label"
        Me.test2Label.Size = New System.Drawing.Size(126, 13)
        Me.test2Label.TabIndex = 1
        Me.test2Label.Text = "Enter the score for test 2:"
        '
        'test3Label
        '
        Me.test3Label.AutoSize = True
        Me.test3Label.Location = New System.Drawing.Point(12, 74)
        Me.test3Label.Name = "test3Label"
        Me.test3Label.Size = New System.Drawing.Size(126, 13)
        Me.test3Label.TabIndex = 2
        Me.test3Label.Text = "Enter the score for test 3:"
        '
        'test1TextBox
        '
        Me.test1TextBox.Location = New System.Drawing.Point(144, 17)
        Me.test1TextBox.Name = "test1TextBox"
        Me.test1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.test1TextBox.TabIndex = 3
        '
        'test2TextBox
        '
        Me.test2TextBox.Location = New System.Drawing.Point(144, 44)
        Me.test2TextBox.Name = "test2TextBox"
        Me.test2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.test2TextBox.TabIndex = 4
        '
        'test3TextBox
        '
        Me.test3TextBox.Location = New System.Drawing.Point(144, 71)
        Me.test3TextBox.Name = "test3TextBox"
        Me.test3TextBox.Size = New System.Drawing.Size(100, 20)
        Me.test3TextBox.TabIndex = 5
        '
        'resultLabel
        '
        Me.resultLabel.AutoSize = True
        Me.resultLabel.Location = New System.Drawing.Point(70, 122)
        Me.resultLabel.Name = "resultLabel"
        Me.resultLabel.Size = New System.Drawing.Size(47, 13)
        Me.resultLabel.TabIndex = 6
        Me.resultLabel.Text = "Average"
        '
        'averageLabel
        '
        Me.averageLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.averageLabel.Location = New System.Drawing.Point(123, 118)
        Me.averageLabel.Name = "averageLabel"
        Me.averageLabel.Size = New System.Drawing.Size(100, 23)
        Me.averageLabel.TabIndex = 7
        '
        'calcButton
        '
        Me.calcButton.Location = New System.Drawing.Point(42, 161)
        Me.calcButton.Name = "calcButton"
        Me.calcButton.Size = New System.Drawing.Size(75, 39)
        Me.calcButton.TabIndex = 8
        Me.calcButton.Text = "Calculate Average"
        Me.calcButton.UseVisualStyleBackColor = True
        '
        'exitButton
        '
        Me.exitButton.Location = New System.Drawing.Point(169, 161)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.Size = New System.Drawing.Size(75, 39)
        Me.exitButton.TabIndex = 9
        Me.exitButton.Text = "Exit"
        Me.exitButton.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.exitButton)
        Me.Controls.Add(Me.calcButton)
        Me.Controls.Add(Me.averageLabel)
        Me.Controls.Add(Me.resultLabel)
        Me.Controls.Add(Me.test3TextBox)
        Me.Controls.Add(Me.test2TextBox)
        Me.Controls.Add(Me.test1TextBox)
        Me.Controls.Add(Me.test3Label)
        Me.Controls.Add(Me.test2Label)
        Me.Controls.Add(Me.test1Label)
        Me.Name = "Form1"
        Me.Text = "Test Average"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents test1Label As System.Windows.Forms.Label
    Friend WithEvents test2Label As System.Windows.Forms.Label
    Friend WithEvents test3Label As System.Windows.Forms.Label
    Friend WithEvents test1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents test2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents test3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents resultLabel As System.Windows.Forms.Label
    Friend WithEvents averageLabel As System.Windows.Forms.Label
    Friend WithEvents calcButton As System.Windows.Forms.Button
    Friend WithEvents exitButton As System.Windows.Forms.Button

End Class
