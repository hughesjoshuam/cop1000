# Author: Joshua M. Hughes
# Chapter: 8 Exercise: 6
# Application Title: Days of the Month
# Date: 7/7/2014
#
# The application will assemble two arrays. One array
# will contain the months of the year. The second array
# will contain the days in the month. The application 
# will loop through the month array and the days array
# producing a string that displays the month and the 
# number of days in the month.
# 

#
#
#
#

def main():
    #Initialize the arrays
    aryMonths = ['January','Feburary','March','April','May','June',\
    'July','August','September','October','November','December']
    aryDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    
    #Loop through the arrays displaying the month and number of days
    for index in range(0,12):
        print aryMonths[index],'has',str(aryDays[index]),'days. '
        
main()

