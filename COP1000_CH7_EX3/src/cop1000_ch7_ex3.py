# Author: Joshua M. Hughes
# Chapter: 7 Exercise: 3 
# Application Title: Fat Gram Calculator
# Date: 7/7/2014
#
# The application will request the number of fat grams
# and the number of calories. The input will be used to
# calculate the percentage of calories from fat. When 
# the percentage of calories from fat is determined the 
# program will display a message identifying the food
# as low fat or high fat.

#
#Declare Real numFatGrams
#Declare Real numCalories
#Declare Real numPercentCalorieFromFat
#Declare String strValidation
#

def main():
    #Request input from user
    numFatGrams = input('Enter the number of Fat Grams. ')
    numCalories = input('Enter the number of Calories. ')
    
    #Validate entries
    strValidation = fncInputValidation(numFatGrams, numCalories)
    
    #Return output as error or fat content statement
    if strValidation != True:
        print strValidation
    else:
        print fncFatGramCalculator(numFatGrams, numCalories)
        
def fncInputValidation(numFatGrams, numCalories):
    #Entry Validation return error or True
    if numFatGrams <= 0:
        return 'Fat Grams cannot be zero.'
    elif numCalories <= 0:
        return 'Calories cannot be zero. '
    elif numCalories < (numFatGrams * 9):
        return 'Value entered for Calories cannot be '\
        'less than '+str(numFatGrams * 9)
    else:
        return True

def fncFatGramCalculator(numFatGrams, numCalories):
    #Calculate percent of calories from fat
    numPercentCalorieFromFat = (numFatGrams * 9) / numCalories
    
    #Determine if fat content is high or low
    if numPercentCalorieFromFat < 0.3:
        return 'This food is low fat. '
    else:
        return 'This food may be high in fat. '
    
main()
