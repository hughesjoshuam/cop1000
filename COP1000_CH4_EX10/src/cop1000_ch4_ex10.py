# Author: Joshua M. Hughes
# Chapter: 4 Exercise: 10
# 
# Application Title: Body Mass Index Enhancement
# Date: 6/13/2014
#
# The application will request weight and height 
# from the user. With the weight and height the Body 
# Mass Index will be calculated. The BMI calculation will 
# be displayed and based on the calculated BMI some friendly 
# advice will be offered.


#Declare Integer intWeight
#Declare Integer intHeight
#Declare Integer intBMI
#Declare String strBMIAdvice


def main():
    intWeight=input('Enter weight in pounds: ')
    intHeight=input('Enter height in inches: ')
	
    #Calculate BMI
    intBMI=fncCalculateBMI(intWeight, intHeight)

    #Retrieve BMI Advice
    strBMIAdvice=fncRetrieveBMIAdvice(intBMI)
    
    #Display BMI and BMI Advice
    print 'BMI: ' + str(intBMI) + ' - ' + strBMIAdvice
 

def fncCalculateBMI(intWeight, intHeight):
	#Calculate BMI score and return the score
        return (intWeight * 703) / intHeight**2


def fncRetrieveBMIAdvice(intBMI):
	#Determine the advice based on BMI score and return the string
	if intBMI > 25:
            return 'You should probably go for a walk instead of messing with this application.'
        elif intBMI < 18.5:
            return 'Go ahead and eat the ice cream. You know you want it.'
        elif intBMI <= 25 and intBMI >= 18.5:
            return 'Keep up the good work! Looking good there Hollywood!'
        
        
main()
