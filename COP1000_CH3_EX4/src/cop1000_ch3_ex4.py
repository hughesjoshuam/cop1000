# Author: Joshua M. Hughes
# Chapter: 3 Exercise: 4
# 
# Application Title: Automobile Costs
# Date: 6/13/2014
#
# The application will calculate the total cost associated 
# with automobile expenses. Expenses calculated will be 
# loan payment, insurance, gas, oil, tires, and maintenance. 
# The application will display the monthly total and annual 
# total of each expense and display a total automobile expense 
# per month and per year.


#Declare Real fltLoanPayment
#Declare Real fltLoanPaymentAnnual
#Declare Real fltInsurance
#Declare Real fltInsuranceAnnual
#Declare Real fltGas
#Declare Real fltGasAnnual
#Declare Real fltOil
#Declare Real fltOilAnnual
#Declare Real fltTires
#Declare Real fltTiresAnnual
#Declare Real fltMaintenance
#Declare Real fltMaintenanceAnnual
#Declare Real fltTotalAnnual
#Declare Real fltTotalMonth


#Import currency and setlocale functions from locale module
#to properly format displayed currency amounts.
#NOTE: Float will not display trailing zeros.
import locale
from locale import currency
from locale import setlocale

#Set the default locale to English_United States 
#for currency formatting in display
setlocale(locale.LC_ALL,'English_United States')

def main():
    #Display prompts requesting user to enter amounts for loan payment,
    #insurance, gas, oil, tires, and maintenance. 
    fltLoanPayment = input('Enter monthly charge for Loan Payment: ')
    fltInsurance = input('Enter monthly charge for Insurance: ')
    fltGas = input('Enter monthly charge for Gas: ')
    fltOil = input('Enter monthly charge for Oil: ')
    fltTires = input('Enter monthly charge for Tires: ')
    fltMaintenance = input('Enter monthly charge for Maintenance: ')
    
    #Calculate Annual amounts for loan payment,
    # insurance, gas, oil, tires, and maintenance. 
    fltLoanPaymentAnnual = (fltLoanPayment  * 12)
    fltInsuranceAnnual = (fltInsurance * 12)
    fltGasAnnual = (fltGas  * 12)
    fltOilAnnual = (fltOil * 12)
    fltTiresAnnual = (fltTires * 12)
    fltMaintenanceAnnual = (fltMaintenance  * 12)

    #Calculate total monthly amount
    fltTotalMonth = fltLoanPayment + fltInsurance + fltGas + fltOil + fltTires + fltMaintenance  

    #Calculate total annual amount
    fltTotalAnnual = fltLoanPaymentAnnual + fltInsuranceAnnual + fltGasAnnual + fltOilAnnual + fltTiresAnnual + fltMaintenanceAnnual

    # Display the monthly total and annual total of each expense 
    print '		 Month       Annual'
    print 'Loan Payment:	' + str(locale.currency(fltLoanPayment)) + '       ' + str(locale.currency(fltLoanPaymentAnnual))
    print 'Insurance:	' + str(locale.currency(fltInsurance)) + '       ' +  str(locale.currency(fltInsuranceAnnual))
    print 'Gas:		' + str(locale.currency(fltGas))  + '       ' +  str(locale.currency(fltGasAnnual))
    print 'Oil:		' + str(locale.currency(fltOil)) + '       ' +  str(locale.currency(fltOilAnnual))
    print 'Tires: 		' + str(locale.currency(fltTires)) + '       ' +  str(locale.currency(fltTiresAnnual))
    print 'Maintenance: 	' + str(locale.currency(fltMaintenance))  + '       ' +  str(locale.currency(fltMaintenanceAnnual))

    #Display total automobile expenses per month and per year
    print '----------------------------------------'
    print 'Total Expense:	' + str(locale.currency(fltTotalMonth)) + '       ' +  str(locale.currency(fltTotalAnnual))
    
main()
