# Author: Joshua M. Hughes
# Chapter: 14 Exercise: 3
# Application Title: Personal Information (Address Book)
# Date: 7/8/2014
#
# The application will create a class that will Add, Edit,
# and display personal information. 
# 

#
#

class PersonalInfo:
    def __init__(self, strName, strAddress, strAge, strPhoneNumber):
        self.strName = strName
        self.strAddress = strAddress
        self.strAge = strAge
        self.strPhoneNumber = strPhoneNumber
        
    def setPersonalInfo(self, strName, strAddress, strAge, strPhoneNumber):
        self.strName = strName
        self.strAddress = strAddress
        self.strAge = strAge
        self.strPhoneNumber = strPhoneNumber
        
    def getPersonalInfo(self):
        return self.strName+',',self.strAddress+',',self.strAge+',',\
        self.strPhoneNumber
    
def main():
    #Initialize PersonalInfo objects
    objPersonalInfo1 = PersonalInfo('Joshua', 'Punta Gorda, FL', \
    '34', '941-740-2072')
    objPersonalInfo2 = PersonalInfo('Aimee', 'Punta Gorda, FL', \
    '33', '941-740-2239')
    objPersonalInfo3 = PersonalInfo('Frank', 'Arcadia, FL', \
    '35', 'Unknown')
    
    #Get current personal Info
    print objPersonalInfo1.getPersonalInfo()
    print objPersonalInfo2.getPersonalInfo()
    print objPersonalInfo3.getPersonalInfo()
    
    #Set new personal info
    objPersonalInfo1.setPersonalInfo('Joshua Hughes', 'Punta Gorda, FL 33955', \
    '34', '941-740-2072')
    objPersonalInfo2.setPersonalInfo('Aimee Hughes', 'Punta Gorda, FL 33955', \
    '33', '941-740-2239')
    objPersonalInfo3.setPersonalInfo('Frank Rayborn', 'Arcadia, FL 34266', \
    '35', 'Unknown')
    
    #Get new personal info values
    print objPersonalInfo1.getPersonalInfo()
    print objPersonalInfo2.getPersonalInfo()
    print objPersonalInfo3.getPersonalInfo()
      
main()

