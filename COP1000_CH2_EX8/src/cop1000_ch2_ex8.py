# Author: Joshua M. Hughes
# Chapter: 2 Exercise: 8
# 
# Application Title: Tip & Tax Calculator
# Date: 6/12/2014
#
# The application will calculate the total amount of the meal purchased,
# prompt the user for the % of tip to include, and calculate 7 percent
# sales tax. The application will display each of the amounts as a final 
# result.

#Declare Real fltMealCost 
#Declare Real fltTipPercent
#Declare Real fltTax
#Declare Real fltTip
#Declare Real fltTotal
#Declare Integer intTipSelect
#Declare Integer intManualTip

#Import currency and setlocale functions from locale module
#to properly format displayed currency amounts.
#NOTE: Float will not display trailing zeros.
import locale
from locale import currency
from locale import setlocale

#Set the locale to English United States for 
#Currency and Date formatting.
setlocale(locale.LC_ALL,'English_United States')

def main():
    #Prompt user to enter the total charge of the meal
    fltMealCost = input('How much did the meal cost? ')
    
    #Prompt user to select or enter tip amount and set tip %
    fltTipPercent = fncSetTipPercent()
    
    #Calculate the tax
    fltTax = fncCalculateTax(fltMealCost)
    
    #Calculate the total tip
    fltTip = fncCalculateTip(fltMealCost, fltTipPercent)
    
    #Calculate the total cost of the meal
    fltTotal = fncCalculateTotal(fltMealCost, fltTax, fltTip)
    
    #Display the meal charge, tip, tax, and total amount
    print 'Meal Charge: ' + str(currency(fltMealCost))
    print 'Tip: ' + str(currency(fltTip)) + ' @ ' + str(fltTipPercent) + '% '
    print 'Tax: ' + str(currency(fltTax))
    print 'Total Charge: ' + str(currency(fltTotal))
    
        
def fncSetTipPercent():
    #Display the tip % options to the user and prompt for selection input
    print 'Select Tip %: '
    print 'Enter 1 for 15% '
    print 'Enter 2 for 16% '
    print 'Enter 3 for 18% '
    print 'Enter 4 to manually enter a tip % '
    intTipSelect = input('Enter Tip Selection Now: ')
    
    #Check for valid response
    blnValidResponse = 0
    while blnValidResponse == 0:
        if intTipSelect not in (1,2,3,4):
            #Prompt the user to enter a proper selection
            print 'You did not enter a valid entry! '
            print 'Please, try again. '
            intTipSelect = input('Enter Tip Selection Now: ')
        else:
            blnValidResponse = 1        
            
    #Return the tip % based on the user selection
    if intTipSelect == 1:
        return 15
    elif intTipSelect == 2:
        return 16
    elif intTipSelect == 3:
        return 18
    elif intTipSelect == 4:
        #Prompt the user to enter the percent manually and 
        #return the percentage 
        intManualTip = input('Enter tip percent: ')
        return (intManualTip)
    
                
def fncCalculateTax(fltMealCost):
    #Set sales tax percentage
    fltSalesTax  = 0.07
    
    #Calculate sales tax by multiplying the meal cost by the tax percent 
    #and return the tax amount
    return (fltMealCost*fltSalesTax)
    
    
def fncCalculateTip(fltMealCost, fltTipPercent):
    #Calculate Tip Amount by multiplying meal cost by the selected tip 
    #amount and return total tip
    return (fltMealCost*(fltTipPercent*.01))
    
    
def fncCalculateTotal(fltMealCost, fltTax, fltTip):
    #Calculate total amount by adding meal cost, sales tax, and tip 
    #and return the total
    return (fltMealCost+fltTax+fltTip)
    
#Call Main module and execute application
main()