/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Joshua
 */
import javax.swing.JOptionPane;

public class MyJavaApp extends javax.swing.JFrame {

    /**
     * Creates new form MyJavaApp
     */
    public MyJavaApp() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FullName_TextField = new javax.swing.JTextField();
        TelePhoneNumber_TextField = new javax.swing.JTextField();
        EmailAddress_TextField = new javax.swing.JTextField();
        FullName_Label = new javax.swing.JLabel();
        TelePhoneNumber_Label = new javax.swing.JLabel();
        EmailAddress_Label = new javax.swing.JLabel();
        Submit_Button = new javax.swing.JButton();
        Reset_Button = new javax.swing.JButton();
        Exit_Button = new javax.swing.JButton();
        Intro_Label = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        FullName_TextField.setName("FullName_TextField"); // NOI18N

        TelePhoneNumber_TextField.setName("TelePhoneNumber_TextField"); // NOI18N

        EmailAddress_TextField.setName("EmailAddress_TextField"); // NOI18N

        FullName_Label.setText("Full Name");
        FullName_Label.setName("FullName_Label"); // NOI18N

        TelePhoneNumber_Label.setText("Telephone Number");
        TelePhoneNumber_Label.setName("TelePhoneNumber_Label"); // NOI18N

        EmailAddress_Label.setText("Email Address");
        EmailAddress_Label.setName("EmailAddress_Label"); // NOI18N

        Submit_Button.setText("Submit");
        Submit_Button.setName("Submit_Button"); // NOI18N
        Submit_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Submit_ButtonActionPerformed(evt);
            }
        });

        Reset_Button.setText("Reset");
        Reset_Button.setName("Reset_Button"); // NOI18N
        Reset_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Reset_ButtonActionPerformed(evt);
            }
        });

        Exit_Button.setText("Exit");
        Exit_Button.setName("Exit_Button"); // NOI18N
        Exit_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Exit_ButtonActionPerformed(evt);
            }
        });

        Intro_Label.setText("My Java Application");
        Intro_Label.setName("Intro_Label"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Intro_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(EmailAddress_Label)
                                .addComponent(TelePhoneNumber_Label)
                                .addComponent(FullName_Label))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(FullName_TextField)
                                .addComponent(TelePhoneNumber_TextField)
                                .addComponent(EmailAddress_TextField, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(Submit_Button)
                            .addGap(18, 18, 18)
                            .addComponent(Reset_Button)
                            .addGap(18, 18, 18)
                            .addComponent(Exit_Button))))
                .addContainerGap(101, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(Intro_Label)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(FullName_TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FullName_Label))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TelePhoneNumber_TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TelePhoneNumber_Label))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EmailAddress_TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EmailAddress_Label))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Submit_Button)
                    .addComponent(Reset_Button)
                    .addComponent(Exit_Button))
                .addContainerGap(86, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Exit_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Exit_ButtonActionPerformed
        // Exit the application with status code of zero.
        System.exit(0);
    }//GEN-LAST:event_Exit_ButtonActionPerformed

    private void Reset_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Reset_ButtonActionPerformed
        // Reset all text fields to nothing
        FullName_TextField.setText("");
        EmailAddress_TextField.setText("");
        TelePhoneNumber_TextField.setText("");
        
    }//GEN-LAST:event_Reset_ButtonActionPerformed

    private void Submit_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Submit_ButtonActionPerformed
        // Display entered information
        String myMessage;
        
        myMessage = "Full Name: " + FullName_TextField.getText() + "\n";
        myMessage = myMessage + "TelePhoneNumber: " + TelePhoneNumber_TextField.getText() + "\n";
        myMessage = myMessage + "TelePhoneNumber: " + EmailAddress_TextField.getText() + "\n";        
                
        JOptionPane.showMessageDialog(rootPane, myMessage);      
        
    }//GEN-LAST:event_Submit_ButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MyJavaApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MyJavaApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MyJavaApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MyJavaApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new MyJavaApp().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel EmailAddress_Label;
    private javax.swing.JTextField EmailAddress_TextField;
    private javax.swing.JButton Exit_Button;
    private javax.swing.JLabel FullName_Label;
    private javax.swing.JTextField FullName_TextField;
    private javax.swing.JLabel Intro_Label;
    private javax.swing.JButton Reset_Button;
    private javax.swing.JButton Submit_Button;
    private javax.swing.JLabel TelePhoneNumber_Label;
    private javax.swing.JTextField TelePhoneNumber_TextField;
    // End of variables declaration//GEN-END:variables
}
