/*
 * Joshua M. Hughes
 * Programming Part 5 : Program 2-5
 * 7/27/2014
 * The program will request name, pay rate, and hours worked in a console
 * application. The input will be recorded and redisplayed in the console
 * window.
 */
package program2_5;

/** Import Scanner object for reading input from keyboard to console**/
import java.util.Scanner;

public class GetInput {

    public static void main(String[] args) {
        //Declare keyboard as Scanner object
        Scanner keyboard = new Scanner(System.in);
        
        //Declare variables
        String name;
        double payRate;
        int hours;
        
        //Prompt user for input and store input in variables
        System.out.print("Enter your name: ");
        name = keyboard.nextLine();
        
        System.out.print("Enter your hourly pay rate: ");
        payRate = keyboard.nextDouble();
        
        System.out.print("Enter the number of hours worked: ");
        hours = keyboard.nextInt();
        
        //Display the previously entered information in the console window
        System.out.println("Here are the values that you entered: ");
        System.out.println(name);
        System.out.println(payRate);
        System.out.println(hours);
    }
    
}
