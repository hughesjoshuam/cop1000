# Author: Joshua M. Hughes
# Chapter: 5 Exercise: 12
# Application Title: Factorial of a Number Calculator
# Date: 6/13/2014
# The application will request a the entry of a nonnegative 
# integer, calculate the product of all the nonnegative integers 
# from one through the nonnegative number entered, and then 
# display the factorial of the nonnegative number. 


#Declare Integer intNumber
#Declare Integer intFactorial
#Declare Integer intIndex

def main():
	#Prompt the user to enter a nonnegative number
	intNumber = input('Enter a nonnegative integer: ')

	#Calculate the factorial of the nonnegative number
	intFactorial = fncCalcFactorial(intNumber)

	#Display the factorial of the nonnegative number
	print 'The factorial is: ' + str(intFactorial)


def fncCalcFactorial(intNumber):
	#Initialize intFactorial Variable and set to 1	
        intFactorial = 1
        
        #Initiailze index and set it to 1
        intIndex = 1
        
        #Loop through integers from 1 until the intNumber is reached
        while intIndex <= intNumber:
            
            #Multiply the Factorial by the Index
            intFactorial = intFactorial * intIndex		
            
            #Increase the index by one
            intIndex = intIndex + 1
            
        #Return the factorial of the number
        return intFactorial

main()
