# Author: Joshua M. Hughes
# Chapter: 12 Exercise: 6
# Application Title: Alphabetic Telephone Number Translator
# Date: 7/8/2014
#
# The application will prompt the user to enter a telephone with 
# alphabetic characters. The alphabetic telephone number will 
# then be translated into all numeric characters and displayed 
# on screen.
# 

#
#Declare String strAlphaTelephoneNumber
#Declare String strNumericTelephoneNumber
#Declare Dictionary dctTranslationTable
#

def main():
    #Request user input of Alphabetic Telephone Number
    strAlphaTelephoneNumber = raw_input('Enter an Alphabetic Telephone '\
    'number: ')    
    
    #Translate Alphabetic Telephone Number
    strNumericTelephoneNumber = fncNumberTranslator(strAlphaTelephoneNumber)
      
    #Display translated number
    print 'Alphabetic Telephone Number: '+strAlphaTelephoneNumber
    print 'Numeric Telephone Number: '+str(strNumericTelephoneNumber)

def fncNumberTranslator(strAlphaTelephoneNumber):
    #Initialize Translation Table Dictionary
    dctTranslationTable={'A':2, 'B':2, 'C':2, 'D':3, 'E':3, 'F':3, 'G':4,\
    'H':4, 'I':4, 'J':5, 'K':5, 'L':5, 'M':6, 'N':6, 'O':6, 'P':7, 'Q':7,\
    'R':7, 'S':7, 'T':8, 'U':8, 'V':8, 'W':9, 'X':9, 'Y':9, 'Z':9}
    
    #Initialize strNumericTelephoneNumber Variable
    strNumericTelephoneNumber=''
    
    #Loop through user entry and translate
    for index in range(0,len(strAlphaTelephoneNumber)):
        #Translate Alpha Characters
        if strAlphaTelephoneNumber[index] not in ('-', ' ', '(', ')') \
            and strAlphaTelephoneNumber[index].isalpha() == True:
                strNumericTelephoneNumber = strNumericTelephoneNumber + \
                str(dctTranslationTable[strAlphaTelephoneNumber[index].upper()])
        else:
            #Copy all other characters as entered
            strNumericTelephoneNumber = strNumericTelephoneNumber + \
            strAlphaTelephoneNumber[index]
    
    #Return numeric telphone number
    return strNumericTelephoneNumber

main()

