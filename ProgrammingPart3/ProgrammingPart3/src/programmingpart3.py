# Author: Joshua M. Hughes
# Programming Part 3
# Application Title: Programming Part 3 
# Date: 7/15/2014
#
# The application is a menu driven application
# incorporating each of the exercises from the Programming 
# Part 1 and Programming Part 2 assignments with corrections.
#

#Declare String strtMainMenuChoice

## NOTE: Consolidated Import and locale for all exercises ##

#Import random for random number generator
import random
#Import locale for setlocale and currency
import locale
#Import currency and setlocale functions from locale module
#to properly format displayed currency amounts.
#NOTE: Float will not display trailing zeros.
from locale import currency
from locale import setlocale
#Import find function from string module
from string import find

#Set the default locale to English_United States 
#for currency formatting in display
setlocale(locale.LC_ALL,'English_United States')

### Chapter: 2 Exercise: 8 ###
### Application Title: Tip & Tax Calculator ###
## The application will calculate the total amount of the meal purchased,
## prompt the user for the % of tip to include, and calculate 7 percent
## sales tax. The application will display each of the amounts as a final 
## result.

#Declare Float fltMealCost 
#Declare Float fltTipPercent
#Declare Float fltTax
#Declare Float fltTip
#Declare Float fltTotal
#Declare Integer intTipSelect
#Declare Integer intManualTip

def c2e8Main():
    #Prompt user to enter the total charge of the meal
    fltMealCost = input('How much did the meal cost? ')
    
    #Prompt user to select or enter tip amount and set tip %
    fltTipPercent = fncSetTipPercent()
    
    #Calculate the tax
    fltTax = fncCalculateTax(fltMealCost)
    
    #Calculate the total tip
    fltTip = fncCalculateTip(fltMealCost, fltTipPercent)
    
    #Calculate the total cost of the meal
    fltTotal = fncCalculateTotal(fltMealCost, fltTax, fltTip)
    
    #Display the meal charge, tip, tax, and total amount
    print 'Meal Charge: ' + str(currency(fltMealCost))
    print 'Tip: ' + str(currency(fltTip)) + ' @ ' + str(fltTipPercent) + '% '
    print 'Tax: ' + str(currency(fltTax))
    print 'Total Charge: ' + str(currency(fltTotal))
    
        
def fncSetTipPercent():
    #Display the tip % options to the user and prompt for selection input
    print 'Select Tip %: '
    print 'Enter 1 for 15% '
    print 'Enter 2 for 16% '
    print 'Enter 3 for 18% '
    print 'Enter 4 to manually enter a tip % '
    intTipSelect = input('Enter Tip Selection Now: ')
    
    #Check for valid response
    blnValidResponse = 0
    while blnValidResponse == 0:
        if intTipSelect not in (1, 2, 3, 4):
            #Prompt the user to enter a proper selection
            print 'You did not enter a valid entry! '
            print 'Please, try again. '
            intTipSelect = input('Enter Tip Selection Now: ')
        else:
            blnValidResponse = 1        
            
    #Return the tip % based on the user selection
    if intTipSelect == 1:
        return 15
    elif intTipSelect == 2:
        return 16
    elif intTipSelect == 3:
        return 18
    elif intTipSelect == 4:
        #Prompt the user to enter the percent manually and 
        #return the percentage 
        intManualTip = input('Enter tip percent: ')
        return (intManualTip)
    
                
def fncCalculateTax(fltMealCost):
    #Set sales tax percentage
    fltSalesTax  = 0.07
    
    #Calculate sales tax by multiplying the meal cost by the tax percent 
    #and return the tax amount
    return (fltMealCost * fltSalesTax)
    
    
def fncCalculateTip(fltMealCost, fltTipPercent):
    #Calculate Tip Amount by multiplying meal cost by the selected tip 
    #amount and return total tip
    return (fltMealCost * (fltTipPercent * .01))
    
    
def fncCalculateTotal(fltMealCost, fltTax, fltTip):
    #Calculate total amount by adding meal cost, sales tax, and tip 
    #and return the total
    return (fltMealCost + fltTax + fltTip)
### END ###

### Chapter: 3 Exercise: 4 ###
### Application Title: Automobile Costs ###
## The application will calculate the total cost associated 
## with automobile expenses. Expenses calculated will be 
## loan payment, insurance, gas, oil, tires, and maintenance. 
## The application will display the monthly total and annual 
## total of each expense and display a total automobile expense 
## per month and per year.

#Declare Float fltLoanPayment
#Declare Float fltLoanPaymentAnnual
#Declare Float fltInsurance
#Declare Float fltInsuranceAnnual
#Declare Float fltGas
#Declare Float fltGasAnnual
#Declare Float fltOil
#Declare Float fltOilAnnual
#Declare Float fltTires
#Declare Float fltTiresAnnual
#Declare Float fltMaintenance
#Declare Float fltMaintenanceAnnual
#Declare Float fltTotalAnnual
#Declare Float fltTotalMonth

def c3e4Main():
    #Display prompts requesting user to enter amounts for loan payment,
    #insurance, gas, oil, tires, and maintenance. 
    fltLoanPayment = input('Enter monthly charge for Loan Payment: ')
    fltInsurance = input('Enter monthly charge for Insurance: ')
    fltGas = input('Enter monthly charge for Gas: ')
    fltOil = input('Enter monthly charge for Oil: ')
    fltTires = input('Enter monthly charge for Tires: ')
    fltMaintenance = input('Enter monthly charge for Maintenance: ')
    
    #Calculate Annual amounts for loan payment,
    # insurance, gas, oil, tires, and maintenance. 
    fltLoanPaymentAnnual = (fltLoanPayment  * 12)
    fltInsuranceAnnual = (fltInsurance * 12)
    fltGasAnnual = (fltGas  * 12)
    fltOilAnnual = (fltOil * 12)
    fltTiresAnnual = (fltTires * 12)
    fltMaintenanceAnnual = (fltMaintenance  * 12)

    #Calculate total monthly amount
    fltTotalMonth = fltLoanPayment + fltInsurance + fltGas + fltOil + \
        fltTires + fltMaintenance  

    #Calculate total annual amount
    fltTotalAnnual = fltLoanPaymentAnnual + fltInsuranceAnnual + \
        fltGasAnnual + fltOilAnnual + fltTiresAnnual + fltMaintenanceAnnual

    # Display the monthly total and annual total of each expense 
    print '		 Month       Annual'
    print 'Loan Payment:	' + str(locale.currency(fltLoanPayment)) \
        + '       ' + str(locale.currency(fltLoanPaymentAnnual))
    print 'Insurance:	' + str(locale.currency(fltInsurance)) \
        + '       ' +  str(locale.currency(fltInsuranceAnnual))
    print 'Gas:		' + str(locale.currency(fltGas))  \
        + '       ' +  str(locale.currency(fltGasAnnual))
    print 'Oil:		' + str(locale.currency(fltOil)) \
        + '       ' +  str(locale.currency(fltOilAnnual))
    print 'Tires: 		' + str(locale.currency(fltTires)) \
        + '       ' +  str(locale.currency(fltTiresAnnual))
    print 'Maintenance: 	' + str(locale.currency(fltMaintenance))  \
        + '       ' +  str(locale.currency(fltMaintenanceAnnual))

    #Display total automobile expenses per month and per year
    print '----------------------------------------'
    print 'Total Expense:	' + str(locale.currency(fltTotalMonth)) \
        + '       ' +  str(locale.currency(fltTotalAnnual))
### END ###

### Chapter: 4 Exercise: 10 ###
### Application Title: Body Mass Index Enhancement ###
## The application will request weight and height 
## from the user. With the weight and height the Body 
## Mass Index will be calculated. The BMI calculation will 
## be displayed and based on the calculated BMI some friendly 
## advice will be offered.

#Declare Integer intWeight
#Declare Integer intHeight
#Declare Integer intBMI
#Declare String strBMIAdvice

def c4e10Main():
    intWeight=input('Enter weight in pounds: ')
    intHeight=input('Enter height in inches: ')
	
    #Calculate BMI
    intBMI=fncCalculateBMI(intWeight, intHeight)

    #Retrieve BMI Advice
    strBMIAdvice=fncRetrieveBMIAdvice(intBMI)
    
    #Display BMI and BMI Advice
    print 'BMI: ' + str(intBMI) + ' - ' + strBMIAdvice
 

def fncCalculateBMI(intWeight, intHeight):
	#Calculate BMI score and return the score
        return (intWeight * 703) / intHeight**2


def fncRetrieveBMIAdvice(intBMI):
	#Determine the advice based on BMI score and return the string
	if intBMI > 25:
            return 'You should probably go for a walk instead of messing \
with this application.'
        elif intBMI < 18.5:
            return 'Go ahead and eat the ice cream. You know you want it.'
        elif intBMI <= 25 and intBMI >= 18.5:
            return 'Keep up the good work! Looking good there Hollywood!'
### END ###

### Chapter: 5 Exercise: 12 ### 
### Application Title: Factorial of a Number Calculator ### 
## The application will request a the entry of a nonnegative 
## integer, calculate the product of all the nonnegative integers 
## from one through the nonnegative number entered, and then 
## display the factorial of the nonnegative number. 

#Declare Integer intNumber
#Declare Integer intFactorial
#Declare Integer intIndex

def c5e12Main():
	#Prompt the user to enter a nonnegative number
	intNumber = input('Enter a nonnegative integer: ')

	#Calculate the factorial of the nonnegative number
	intFactorial = fncCalcFactorial(intNumber)

	#Display the factorial of the nonnegative number
	print 'The factorial is: ' + str(intFactorial)


def fncCalcFactorial(intNumber):
	#Initialize intFactorial Variable and set to 1	
        intFactorial = 1
        
        #Initiailze index and set it to 1
        intIndex = 1
        
        #Loop through integers from 1 until the intNumber is reached
        while intIndex <= intNumber:
            
            #Multiply the Factorial by the Index
            intFactorial = intFactorial * intIndex		
            
            #Increase the index by one
            intIndex = intIndex + 1
            
        #Return the factorial of the number
        return intFactorial
### END ###

### Chapter: 6 Exercise: 12 ### 
### Application Title: Slot Machine (Lucky 7) ### 
## The application will create an dictionary with seven slot 
## machine charms.  The user will be given 100 credits. 
## The user will be prompted to select the number of 
## credits to gamble in 10, 20, 30, or 40 increments. 
## After the user selects the number credits to gamble, 
## the slot machine will use the random number generator 
## to choose seven charms from the charms dictiondct. The 
## user must get three or more charms to earn credits.  
## If the user has no credits left the game is lost. 

#Declare Integer intCredits
#Declare Integer intGambleCredits
#Declare Integer intCreditsEarned
#Declare Integer intRandomNumber
#Declare Integer index
#Declare Integer intDiamond 
#Declare Integer intCrystal 
#Declare Integer intEmerald 
#Declare Integer intSapphire 
#Declare Integer intRuby 
#Declare Integer intOpal 
#Declare Integer intQuartz 
#Declare Integer intCharmesMatched
#Declare String strContinue
#Declare String strPlayAgain
#Constant Integer SIZE = 6

def c6e12Main():
    #Introduce the game
    print 'Welcome to Lucky7 '
        
    #Set starting credits for the user
    intCredits = 100
    print 'Starting Credits: ' + str(intCredits)

    #Set Continue Game to True
    strContinue = 'true'
    
    while strContinue == 'true':
        #Prompt the user to select the number of credits to gamble.
        print 'Enter 1 to play 10 Credits '
        print 'Enter 2 to play 20 Credits '
        print 'Enter 3 to play 30 Credits '
        print 'Enter 4 to play 40 Credits '
        
        intGambleCredits = input('Select the number of credits to play: ')

        #Subtract Gambled Credits from the total credits
        if intGambleCredits == 1:
            intCredits = intCredits - 5
        elif intGambleCredits == 2:
            intCredits = intCredits - 10
        elif intGambleCredits == 3:
            intCredits = intCredits - 15
        elif intGambleCredits == 4:
            intCredits = intCredits - 20
            
        #Select seven random charms
        intCreditsEarned = fncRandomCharmGenerator()
        
        #The earned credits will be multiplied by 1, 2, 3, or 4 
        #depending on the number of credits gambled.
        intCreditsEarned = intCreditsEarned * intGambleCredits
        
        #After each turn the seven charms and the amount of available 
        #credits will be displayed.
        print 'Credits Won: ' + str(intCreditsEarned)
        
        #Add earned credits to total credits
        intCredits = intCredits + intCreditsEarned
        
        print 'Remaining Credits: ' + str(intCredits)
             
        #The user will be prompted to keep playing or end the game. 
        #Play again or not? Choice or out of credits
        if intCredits > 0:
            strPlayAgain = raw_input('Do you want to play again (Y or N): ')

            if strPlayAgain == 'Y' or strPlayAgain == 'y':
                    strContinue = 'true'
            else:
                    strContinue = 'false'
        else:
            print 'You have no credits remaining. '
            print 'GAME OVER '
            strContinue = 'false'
    #When the game is over the final credit 
    #amount will be displayed.
    print 'Ending Credits: ' + str(intCredits)
    

def fncRandomCharmGenerator():
   
    #Create Charms Dictionary
    dctCharms={0:'Diamond', 1:'Crystal', 2:'Emerald', 3:'Sapphire', 
    4:'Ruby', 5:'Opal', 6:'Quartz'}

    #Create Selected Charms Dictionary
    dctSelectCharms={0:''}

    #Loop seven times and select seven random charms
    for index in range (7):
        intRandomNumber = random.randint(0, 6)
        dctSelectCharms[index] = dctCharms[intRandomNumber]
        print dctSelectCharms[index]
 
    #Initialize charms variables
    intDiamond = 0
    intCrystal = 0
    intEmerald = 0
    intSapphire = 0
    intRuby = 0
    intOpal = 0
    intQuartz = 0
    
    #Loop through selected charms array and calculate number 
    #of matched charms
    for index in range (7):
        if dctSelectCharms[index] == 'Diamond':
            intDiamond = intDiamond +1 
        elif dctSelectCharms[index] == 'Crystal':
            intCrystal = intCrystal+1
        elif dctSelectCharms[index] == 'Emerald':
            intEmerald= intEmerald +1
        elif dctSelectCharms[index] == 'Sapphire':
            intSapphire= intSapphire +1
        elif dctSelectCharms[index] == 'Ruby':
            intRuby= intRuby +1
        elif dctSelectCharms[index] == 'Opal':
            intOpal= intOpal +1
        elif dctSelectCharms[index] == 'Quartz':
            intQuartz= intQuartz +1
	
    #Initialize number of charms matched and set to zero
    intCharmesMatched = 0

    #Calculate total number of matches
    if intDiamond >= 2:
        intCharmesMatched = intCharmesMatched + intDiamond
    if intCrystal >= 2:
        intCharmesMatched = intCharmesMatched + intCrystal
    if intEmerald >= 2:
	intCharmesMatched = intCharmesMatched + intEmerald
    if intSapphire >= 2:
	intCharmesMatched = intCharmesMatched + intSapphire
    if intRuby >= 2:
	intCharmesMatched = intCharmesMatched + intRuby
    if intOpal >= 2:
	intCharmesMatched = intCharmesMatched + intOpal
    if intQuartz >= 2:
	intCharmesMatched = intCharmesMatched + intQuartz

    #Calculate earnings based on total matches. Three charms = 2 credits, 
    #four charms = 5 credits, five charms = 10 credits, 
    #six charms = 15 credits, and seven charms = 20 credits
    if intCharmesMatched == 3:
	intCreditsEarned = 2
    elif intCharmesMatched == 4:
	intCreditsEarned = 5
    elif intCharmesMatched == 5:
	intCreditsEarned = 10 
    elif intCharmesMatched == 6:
	intCreditsEarned = 15
    elif intCharmesMatched == 7:
	intCreditsEarned = 20
    else:
        intCreditsEarned = 0
        
     #Debugging program by displaying charms
    print dctCharms
    print dctSelectCharms
        
    return intCreditsEarned
### END ###

### Chapter: 7 Exercise: 3  ### 
### Application Title: Fat Gram Calculator ### 
## The application will request the number of fat grams
## and the number of calories. The input will be used to
## calculate the percentage of calories from fat. When 
## the percentage of calories from fat is determined the 
## program will display a message identifying the food
## as low fat or high fat.

#Declare Real numFatGrams
#Declare Real numCalories
#Declare Real numPercentCalorieFromFat
#Declare String strValidation

def c7e3Main():
    #Initialize strValidation to False
    strValidation = False
    
    while strValidation != True:
        #Request input from user
        numFatGrams = input('Enter the number of Fat Grams. ')
        numCalories = input('Enter the number of Calories. ')
        
        #Validate entries
        strValidation = fncInputValidation(numFatGrams, numCalories)
    
        #Return output as error or fat content statement
        if strValidation != True:
            print strValidation
        else:
            print fncFatGramCalculator(numFatGrams, numCalories)
        
def fncInputValidation(numFatGrams, numCalories):
    #Entry Validation return error or True
    if numFatGrams <= 0:
        return 'Fat Grams cannot be zero.'
    elif numCalories <= 0:
        return 'Calories cannot be zero. '
    elif numCalories < (numFatGrams * 9):
        return 'Value entered for Calories cannot be '\
        'less than '+str(numFatGrams * 9)
    else:
        return True

def fncFatGramCalculator(numFatGrams, numCalories):
    #Calculate percent of calories from fat
    numPercentCalorieFromFat = (numFatGrams * 9) / numCalories
    
    #Determine if fat content is high or low
    if numPercentCalorieFromFat < 0.3:
        return 'This food is low fat. '
    else:
        return 'This food may be high in fat. '
### END ###

### Chapter: 8 Exercise: 6 ### 
### Application Title: Days of the Month ### 
## The application will assemble two arrays. One array
## will contain the months of the year. The second array
## will contain the days in the month. The application 
## will loop through the month array and the days array
## producing a string that displays the month and the 
## number of days in the month.

def c8e6Main():
    #Initialize the arrays
    aryMonths = ['January', 'Feburary', 'March', 'April', 'May', 'June',\
    'July', 'August', 'September', 'October', 'November', 'December']
    aryDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    
    #Loop through the arrays displaying the month and number of days
    for index in range(0,12):
        print aryMonths[index], 'has', str(aryDays[index]), 'days. '
### END ###

### Chapter: 10 Exercise: 6 ### 
### Application Title: Golf Scores ### 
## The application will record golf players names and scores
## into a file named golf.dat. Each record stored in the file
## will contain a name and a score. The application will edit
## records, delete records, and read records from golf.dat. The 
## records read from the file will be displayed on the screen.

#Declare FileObject golf_dat
#Declare String strName
#Declare String strRecord
#Declare Integer intScore
#Declare Integer intMenuOption

def c10e6Main():
    #Initialize menu option variable
    intMenuOption = 0
    
    #Begin Application Loop
    while intMenuOption != 4:
        #If file does not exist, initialize blank file
        golf_dat = open('golf.dat', 'a')
        golf_dat.close()
        
        #Display Current Golf Scores
        modDisplayGolfScores()
        
        #Display menu and return selected option
        intMenuOption = fncDisplayMenu()
        
        #Perform selected action
        if intMenuOption == 1:
            modAddAPlayer()
        elif intMenuOption == 2:
            modEditGolfScore()
        elif intMenuOption == 3:
            modDeleteGolfPlayer()
        elif intMenuOption == 4:
            break

def fncDisplayMenu():
    #Display application options
    print 'Choose an Option: '
    print '1: Add a Player '
    print '2: Edit a Score '
    print '3: Delete a Player '
    print '4: Exit '
    intMenuOption = input()
    
    #Validate selected option
    while intMenuOption not in (1, 2, 3, 4):
        print 'That is not a valid selection. '
        print 'Choose an Option: '
        print '1: Add a Player '
        print '2: Edit a Score '
        print '3: Delete a Player '
        print '4: Exit '
        intMenuOption = input() 
    
    #Return validated selection
    return intMenuOption
    
def modDisplayGolfScores():
    #Open file in read only
    golf_dat = open('golf.dat', 'r')
    
    #Reaf first line from file
    strRecord = golf_dat.readline()
    
    print 'Current Golf Scores: '
    print 'Player : Score'
    while strRecord != '':
        #Initialize blank variables
        strName = ''
        intScore = ''
        
        #Read Players and Scores from record
        for index in range(0, find(strRecord, ',')):
            strName = strName+strRecord[index]
        for index in range(find(strRecord, ',')+1, len(strRecord)-1):
            intScore = str(intScore)+str(strRecord[index])
        
        #Display Player and Score
        print strName,':',str(intScore)

        #Read next record from file
        strRecord = golf_dat.readline()
        
    #Close the file
    golf_dat.close()
    
    print ''
    
def modAddAPlayer():
    #Request user input of player information
    strName = raw_input('Enter player name: ')
    intScore = input('Enter player score: ')
    
    #Open file for write
    golf_dat = open('golf.dat', 'a')
    
    #Write record to file
    golf_dat.write(strName+','+str(intScore)+'\n')
    
    #Close file
    golf_dat.close()
        
    print ''
    
def modEditGolfScore():
    #Initialize golf scores and players arrays
    aryPlayers = []
    aryGolfScores = []
        
    #Open file in read only
    golf_dat = open('golf.dat', 'r')
    
    #Reaf first line from file
    strRecord = golf_dat.readline()

    #Read through each record in file until EOF
    while strRecord != '':
        #Initialize blank variables
        strName = ''
        intScore = ''
        
        #Read Players and Scores from record
        for index in range(0, find(strRecord, ',')):
            strName = strName+strRecord[index]
        for index in range(find(strRecord, ',')+1, len(strRecord)-1):
            intScore = str(intScore)+str(strRecord[index])
            
        #Puplate array with players
        aryPlayers.append(strName)
        
        #Populate array with scores
        aryGolfScores.append(intScore)
        
        #Read next record from file
        strRecord = golf_dat.readline()
        
    #Close the file
    golf_dat.close()
    
    #Display Players and display choice selection for delete
    for index in range(0, len(aryPlayers)):
        print index,':',aryPlayers[index]
    
    #Prompt user to select player to delete
    index = input('Choose a player to edit: ')
    
    #Prompt user to enter new score
    intScore = input('Enter new golf score: ')
    
    #Remove player and score from arrays
    aryGolfScores[index] = intScore
   
    #Open file in write mode
    golf_dat = open('golf.dat', 'w')
    
    #Recreate file using modified players and scores
    for index in range(0, len(aryPlayers)):
        #Write record to file
        golf_dat.write(aryPlayers[index]+','+str(aryGolfScores[index])+'\n')
    
    #Close file
    golf_dat.close()
            
    print ''
    
def modDeleteGolfPlayer():
    #Initialize golf scores and players arrays
    aryPlayers = []
    aryGolfScores = []
        
    #Open file in read only
    golf_dat = open('golf.dat', 'r')
    
    #Reaf first line from file
    strRecord = golf_dat.readline()

    #Read through each record in file until EOF
    while strRecord != '':
        #Initialize blank variables
        strName = ''
        intScore = ''
        
        #Read Players and Scores from record
        for index in range(0, find(strRecord, ',')):
            strName = strName+strRecord[index]
        for index in range(find(strRecord, ',')+1, len(strRecord)-1):
            intScore = str(intScore)+str(strRecord[index])
            
        #Puplate array with players
        aryPlayers.append(strName)
        
        #Populate array with scores
        aryGolfScores.append(intScore)
        
        #Read next record from file
        strRecord = golf_dat.readline()
        
    #Close the file
    golf_dat.close()
    
    #Display Players and display choice selection for delete
    for index in range(0, len(aryPlayers)):
        print index,':',aryPlayers[index]
    
    #Prompt user to select player to delete
    index = input('Choose a player to delete: ')
    
    #Remove player and score from arrays
    aryPlayers.pop(index)
    aryGolfScores.pop(index)
   
    #Open file in write mode
    golf_dat = open('golf.dat', 'w')
    
    #Recreate file using modified players and scores
    for index in range(0, len(aryPlayers)):
        #Write record to file
        golf_dat.write(aryPlayers[index]+','+str(aryGolfScores[index])+'\n')
    
    #Close file
    golf_dat.close()
        
    print ''
### END ###

### Chapter: 12 Exercise: 6 ### 
### Application Title: Alphabetic Telephone Number Translator ### 
## The application will prompt the user to enter a telephone with 
## alphabetic characters. The alphabetic telephone number will 
## then be translated into all numeric characters and displayed 
## on screen.

#Declare String strAlphaTelephoneNumber
#Declare String strNumericTelephoneNumber
#Declare Dictionary dctTranslationTable

def c12e6Main():
    #Request user input of Alphabetic Telephone Number
    strAlphaTelephoneNumber = raw_input('Enter an Alphabetic Telephone '\
    'number: ')    
    
    #Translate Alphabetic Telephone Number
    strNumericTelephoneNumber = fncNumberTranslator(strAlphaTelephoneNumber)
      
    #Display translated number
    print 'Alphabetic Telephone Number: '+strAlphaTelephoneNumber
    print 'Numeric Telephone Number: '+str(strNumericTelephoneNumber)

def fncNumberTranslator(strAlphaTelephoneNumber):
    #Initialize Translation Table Dictionary
    dctTranslationTable={'A':2, 'B':2, 'C':2, 'D':3, 'E':3, 'F':3, 'G':4,\
    'H':4, 'I':4, 'J':5, 'K':5, 'L':5, 'M':6, 'N':6, 'O':6, 'P':7, 'Q':7,\
    'R':7, 'S':7, 'T':8, 'U':8, 'V':8, 'W':9, 'X':9, 'Y':9, 'Z':9}
    
    #Initialize strNumericTelephoneNumber Variable
    strNumericTelephoneNumber=''
    
    #Loop through user entry and translate
    for index in range(0,len(strAlphaTelephoneNumber)):
        #Translate Alpha Characters
        if strAlphaTelephoneNumber[index] not in ('-', ' ', '(', ')') \
            and strAlphaTelephoneNumber[index].isalpha() == True:
                strNumericTelephoneNumber = strNumericTelephoneNumber + \
                str(dctTranslationTable[strAlphaTelephoneNumber[index].upper()])
        else:
            #Copy all other characters as entered
            strNumericTelephoneNumber = strNumericTelephoneNumber + \
            strAlphaTelephoneNumber[index]
    
    #Return numeric telphone number
    return strNumericTelephoneNumber
### END ###

### Chapter: 14 Exercise: 3 ### 
### Application Title: Personal Information (Address Book) ### 
## The application will create a class that will Add, Edit,
## and display personal information. 

class PersonalInfo:
    def __init__(self, strName, strAddress, strAge, strPhoneNumber):
        self.strName = strName
        self.strAddress = strAddress
        self.strAge = strAge
        self.strPhoneNumber = strPhoneNumber
        
    def setPersonalInfo(self, strName, strAddress, strAge, strPhoneNumber):
        self.strName = strName
        self.strAddress = strAddress
        self.strAge = strAge
        self.strPhoneNumber = strPhoneNumber
        
    def getPersonalInfo(self):
        return self.strName+',',self.strAddress+',',self.strAge+',',\
        self.strPhoneNumber
    
def c14e3Main():
    #Initialize PersonalInfo objects
    objPersonalInfo1 = PersonalInfo('Joshua', 'Punta Gorda, FL', \
    '34', '941-740-2072')
    objPersonalInfo2 = PersonalInfo('Aimee', 'Punta Gorda, FL', \
    '33', '941-740-2239')
    objPersonalInfo3 = PersonalInfo('Frank', 'Arcadia, FL', \
    '35', 'Unknown')
    
    #Get current personal Info
    print objPersonalInfo1.getPersonalInfo()
    print objPersonalInfo2.getPersonalInfo()
    print objPersonalInfo3.getPersonalInfo()
    
    #Set new personal info
    objPersonalInfo1.setPersonalInfo('Joshua Hughes', 'Punta Gorda, FL 33955', \
    '34', '941-740-2072')
    objPersonalInfo2.setPersonalInfo('Aimee Hughes', 'Punta Gorda, FL 33955', \
    '33', '941-740-2239')
    objPersonalInfo3.setPersonalInfo('Frank Rayborn', 'Arcadia, FL 34266', \
    '35', 'Unknown')
    
    #Get new personal info values
    print objPersonalInfo1.getPersonalInfo()
    print objPersonalInfo2.getPersonalInfo()
    print objPersonalInfo3.getPersonalInfo()
### END ###

#Define Main module for menu driven application
def main():
    #Initialize menu choice to zero
    strtMainMenuChoice = 0
    
    #Loop through Main Menu until exit
    while strtMainMenuChoice != 11:
        print 'Enter a number to run an application: '
        print '1: Tip & Tax Calculator '
        print '2: Automobile Costs '
        print '3: Body Mass Index Enhancement '
        print '4: Factorial of a Number Calculator '
        print '5: Slot Machine (Lucky 7) '
        print '6: Fat Gram Calculator '
        print '7: Days of the Month '
        print '8: Golf Scores '
        print '9: Alphabetic Telephone Number Translator '
        print '10: Personal Information (Address Book) '
        print '11: Exit '
        strtMainMenuChoice = raw_input()
        
        #Choice Validation
        if strtMainMenuChoice.isdigit() == True and \
            int(strtMainMenuChoice) in range(1,12):
            if int(strtMainMenuChoice) == 1:
                c2e8Main()
            elif int(strtMainMenuChoice) == 2:
                c3e4Main()
            elif int(strtMainMenuChoice) == 3:
                c4e10Main()
            elif int(strtMainMenuChoice) == 4:
                c5e12Main()
            elif int(strtMainMenuChoice) == 5:
                c6e12Main()
            elif int(strtMainMenuChoice) == 6:
                c7e3Main()
            elif int(strtMainMenuChoice) == 7:
                c8e6Main()
            elif int(strtMainMenuChoice) == 8:
                c10e6Main()
            elif int(strtMainMenuChoice) == 9:
                c12e6Main()
            elif int(strtMainMenuChoice) == 10:
                c14e3Main()
            elif int(strtMainMenuChoice) == 11:
                print 'Good Bye. '
                #End Main application
                break
        else:
            print strtMainMenuChoice, 'is not a valid selection. '
    
#Call Main module and execute application
main()
