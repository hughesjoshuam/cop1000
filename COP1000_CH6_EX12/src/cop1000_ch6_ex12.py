# Author: Joshua M. Hughes
# Chapter: 6 Exercise: 12
# Application Title: Slot Machine  (Lucky 7)
# Date: 6/13/2014
#
# The application will create an dictionary with seven slot 
# machine charms.  The user will be given 100 credits. 
# The user will be prompted to select the number of 
# credits to gamble in 10, 20, 30, or 40 increments. 
# After the user selects the number credits to gamble, 
# the slot machine will use the random number generator 
# to choose seven charms from the charms dictiondct. The 
# user must get three or more charms to earn credits. 
# Three charms = 2 credits, four charms = 5 credits, five 
# charms = 10 credits, six charms = 15 credits, and seven 
# charms = 20 credits. The earned credits will be multiplied 
# by 1,2,3,or 4 depending on the number of credits gambled. 
# After each turn the seven charms and the amount of available 
# credits will be displayed. The user will be prompted to 
# keep playing or end the game. If the user has no credits left 
# the game is lost. When the game is over the final credit 
# amount will be displayed.



#Declare Integer intCredits
#Declare Integer intGambleCredits
#Declare Integer intCreditsEarned
#Declare Integer intRandomNumber
#Declare Integer index
#Declare Integer intDiamond 
#Declare Integer intCrystal 
#Declare Integer intEmerald 
#Declare Integer intSapphire 
#Declare Integer intRuby 
#Declare Integer intOpal 
#Declare Integer intQuartz 
#Declare Integer intCharmesMatched
#Declare String strContinue
#Declare String strPlayAgain
#Constant Integer SIZE = 6

#Import Random Number Module to 
import random

def main():
    #Introduce the game
    print 'Welcome to Lucky7 '
        
    #Set starting credits for the user
    intCredits = 100
    print 'Starting Credits: ' + str(intCredits)

    #Set Continue Game to True
    strContinue = 'true'
    
    while strContinue == 'true':
        #Prompt the user to select the number of credits to gamble.
        print 'Enter 1 to play 10 Credits '
        print 'Enter 2 to play 20 Credits '
        print 'Enter 3 to play 30 Credits '
        print 'Enter 4 to play 40 Credits '
        
        intGambleCredits = input('Select the number of credits to play: ')

        #Subtract Gambled Credits from the total credits
        if intGambleCredits == 1:
            intCredits = intCredits - 5
        elif intGambleCredits == 2:
            intCredits = intCredits - 10
        elif intGambleCredits == 3:
            intCredits = intCredits - 15
        elif intGambleCredits == 4:
            intCredits = intCredits - 20
            
        #Select seven random charms
        intCreditsEarned = fncRandomCharmGenerator()
        intCreditsEarned = intCreditsEarned * intGambleCredits
        print 'Credits Won: ' + str(intCreditsEarned)
        
        #Add earned credits to total credits
        intCredits = intCredits + intCreditsEarned
        
        print 'Remaining Credits: ' + str(intCredits)
             
        #Play again or not? Choice or out of credits
        if intCredits > 0:
            strPlayAgain = raw_input('Do you want to play again (Y or N): ')

            if strPlayAgain == 'Y' or strPlayAgain == 'y':
                    strContinue = 'true'
            else:
                    strContinue = 'false'
        else:
            print 'You have no credits remaining. '
            print 'GAME OVER '
            strContinue = 'false'
    
    print 'Ending Credits: ' + str(intCredits)
    

def fncRandomCharmGenerator():
   
    #Create Charms Dictionary
    dctCharms={0:'Diamond', 1:'Crystal', 2:'Emerald', 3:'Sapphire', 
    4:'Ruby', 5:'Opal', 6:'Quartz'}

    #Create Selected Charms Dictionary
    dctSelectCharms={0:''}

    #Loop seven times and select seven random charms
    for index in range (7):
        intRandomNumber = random.randint(0, 6)
        dctSelectCharms[index] = dctCharms[intRandomNumber]
        print dctSelectCharms[index]
 
    #Initialize charms variables
    intDiamond = 0
    intCrystal = 0
    intEmerald = 0
    intSapphire = 0
    intRuby = 0
    intOpal = 0
    intQuartz = 0
    
    #Loop through selected charms array and calculate number 
    #of matched charms
    for index in range (7):
        if dctSelectCharms[index] == 'Diamond':
            intDiamond = intDiamond +1 
        elif dctSelectCharms[index] == 'Crystal':
            intCrystal = intCrystal+1
        elif dctSelectCharms[index] == 'Emerald':
            intEmerald= intEmerald +1
        elif dctSelectCharms[index] == 'Sapphire':
            intSapphire= intSapphire +1
        elif dctSelectCharms[index] == 'Ruby':
            intRuby= intRuby +1
        elif dctSelectCharms[index] == 'Opal':
            intOpal= intOpal +1
        elif dctSelectCharms[index] == 'Quartz':
            intQuartz= intQuartz +1
	
    #Initialize number of charms matched and set to zero
    intCharmesMatched = 0

    #Calculate total number of matches
    if intDiamond >= 2:
        intCharmesMatched = intCharmesMatched + intDiamond
    if intCrystal >= 2:
        intCharmesMatched = intCharmesMatched + intCrystal
    if intEmerald >= 2:
	intCharmesMatched = intCharmesMatched + intEmerald
    if intSapphire >= 2:
	intCharmesMatched = intCharmesMatched + intSapphire
    if intRuby >= 2:
	intCharmesMatched = intCharmesMatched + intRuby
    if intOpal >= 2:
	intCharmesMatched = intCharmesMatched + intOpal
    if intQuartz >= 2:
	intCharmesMatched = intCharmesMatched + intQuartz

    #Calculate earnings based on total matches
    if intCharmesMatched == 3:
	intCreditsEarned = 2
    elif intCharmesMatched == 4:
	intCreditsEarned = 5
    elif intCharmesMatched == 5:
	intCreditsEarned = 10 
    elif intCharmesMatched == 6:
	intCreditsEarned = 15
    elif intCharmesMatched == 7:
	intCreditsEarned = 20
    else:
        intCreditsEarned = 0
        
     #Debugging program by displaying charms
    print dctCharms
    print dctSelectCharms
        
    return intCreditsEarned

main()
