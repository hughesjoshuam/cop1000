# Author: Joshua M. Hughes
# Chapter: 10 Exercise: 6
# Application Title: Golf Scores
# Date: 7/7/2014
#
# The application will record golf players names and scores
# into a file named golf.dat. Each record stored in the file
# will contain a name and a score. The application will edit
# records, delete records, and read records from golf.dat. The 
# records read from the file will be displayed on the screen.
# 

#
#Declare FileObject golf_dat
#Declare String strName
#Declare String strRecord
#Declare Integer intScore
#Declare Integer intMenuOption
#

from string import find

def main():
    #Initialize menu option variable
    intMenuOption = 0
    
    #Begin Application Loop
    while intMenuOption != 4:
        #If file does not exist, initialize blank file
        golf_dat = open('golf.dat', 'a')
        golf_dat.close()
        
        #Display Current Golf Scores
        modDisplayGolfScores()
        
        #Display menu and return selected option
        intMenuOption = fncDisplayMenu()
        
        #Perform selected action
        if intMenuOption == 1:
            modAddAPlayer()
        elif intMenuOption == 2:
            modEditGolfScore()
        elif intMenuOption == 3:
            modDeleteGolfPlayer()
        elif intMenuOption == 4:
            break

def fncDisplayMenu():
    #Display application options
    print 'Choose an Option: '
    print '1: Add a Player '
    print '2: Edit a Score '
    print '3: Delete a Player '
    print '4: Exit '
    intMenuOption = input()
    
    #Validate selected option
    while intMenuOption not in (1, 2, 3, 4):
        print 'That is not a valid selection. '
        print 'Choose an Option: '
        print '1: Add a Player '
        print '2: Edit a Score '
        print '3: Delete a Player '
        print '4: Exit '
        intMenuOption = input() 
    
    #Return validated selection
    return intMenuOption
    
def modDisplayGolfScores():
    #Open file in read only
    golf_dat = open('golf.dat', 'r')
    
    #Reaf first line from file
    strRecord = golf_dat.readline()
    
    print 'Current Golf Scores: '
    print 'Player : Score'
    while strRecord != '':
        #Initialize blank variables
        strName = ''
        intScore = ''
        
        #Read Players and Scores from record
        for index in range(0, find(strRecord, ',')):
            strName = strName+strRecord[index]
        for index in range(find(strRecord, ',')+1, len(strRecord)-1):
            intScore = str(intScore)+str(strRecord[index])
        
        #Display Player and Score
        print strName,':',str(intScore)

        #Read next record from file
        strRecord = golf_dat.readline()
        
    #Close the file
    golf_dat.close()
    
    print ''
    
def modAddAPlayer():
    #Request user input of player information
    strName = raw_input('Enter player name: ')
    intScore = input('Enter player score: ')
    
    #Open file for write
    golf_dat = open('golf.dat', 'a')
    
    #Write record to file
    golf_dat.write(strName+','+str(intScore)+'\n')
    
    #Close file
    golf_dat.close()
        
    print ''
    
def modEditGolfScore():
    #Initialize golf scores and players arrays
    aryPlayers = []
    aryGolfScores = []
        
    #Open file in read only
    golf_dat = open('golf.dat', 'r')
    
    #Reaf first line from file
    strRecord = golf_dat.readline()

    #Read through each record in file until EOF
    while strRecord != '':
        #Initialize blank variables
        strName = ''
        intScore = ''
        
        #Read Players and Scores from record
        for index in range(0, find(strRecord, ',')):
            strName = strName+strRecord[index]
        for index in range(find(strRecord, ',')+1, len(strRecord)-1):
            intScore = str(intScore)+str(strRecord[index])
            
        #Puplate array with players
        aryPlayers.append(strName)
        
        #Populate array with scores
        aryGolfScores.append(intScore)
        
        #Read next record from file
        strRecord = golf_dat.readline()
        
    #Close the file
    golf_dat.close()
    
    #Display Players and display choice selection for delete
    for index in range(0, len(aryPlayers)):
        print index,':',aryPlayers[index]
    
    #Prompt user to select player to delete
    index = input('Choose a player to edit: ')
    
    #Prompt user to enter new score
    intScore = input('Enter new golf score: ')
    
    #Remove player and score from arrays
    aryGolfScores[index] = intScore
   
    #Open file in write mode
    golf_dat = open('golf.dat', 'w')
    
    #Recreate file using modified players and scores
    for index in range(0, len(aryPlayers)):
        #Write record to file
        golf_dat.write(aryPlayers[index]+','+str(aryGolfScores[index])+'\n')
    
    #Close file
    golf_dat.close()
            
    print ''
    
def modDeleteGolfPlayer():
    #Initialize golf scores and players arrays
    aryPlayers = []
    aryGolfScores = []
        
    #Open file in read only
    golf_dat = open('golf.dat', 'r')
    
    #Reaf first line from file
    strRecord = golf_dat.readline()

    #Read through each record in file until EOF
    while strRecord != '':
        #Initialize blank variables
        strName = ''
        intScore = ''
        
        #Read Players and Scores from record
        for index in range(0, find(strRecord, ',')):
            strName = strName+strRecord[index]
        for index in range(find(strRecord, ',')+1, len(strRecord)-1):
            intScore = str(intScore)+str(strRecord[index])
            
        #Puplate array with players
        aryPlayers.append(strName)
        
        #Populate array with scores
        aryGolfScores.append(intScore)
        
        #Read next record from file
        strRecord = golf_dat.readline()
        
    #Close the file
    golf_dat.close()
    
    #Display Players and display choice selection for delete
    for index in range(0, len(aryPlayers)):
        print index,':',aryPlayers[index]
    
    #Prompt user to select player to delete
    index = input('Choose a player to delete: ')
    
    #Remove player and score from arrays
    aryPlayers.pop(index)
    aryGolfScores.pop(index)
   
    #Open file in write mode
    golf_dat = open('golf.dat', 'w')
    
    #Recreate file using modified players and scores
    for index in range(0, len(aryPlayers)):
        #Write record to file
        golf_dat.write(aryPlayers[index]+','+str(aryGolfScores[index])+'\n')
    
    #Close file
    golf_dat.close()
        
    print ''
    
main()

